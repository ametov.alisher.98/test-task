import Vue from 'vue'
import App from './App.vue'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'
import VueRouter from 'vue-router'
import HelloWorld from './components/HelloWorld';
import itemData from './components/item-data/itemData';

Vue.config.productionTip = false
Vue.use(ElementUI, { locale })
Vue.use(VueRouter)

const routes = [
  { path: '', component: HelloWorld, },
  { path: '/:id', component: itemData, },
]
const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
